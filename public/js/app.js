var app = {
  init: function () {
    console.log('init');

    $('#formCreate').on('submit', {
      url: "./create/submit"
    }, app.submitForm);

    $('#formSignIn').on('submit', {
      url: "./user/signin"
    }, app.submitForm);

    $('#formSignUp').on('submit', {
      url: "./user/signup"
    }, app.submitForm);

    $('#quizCreate').on('click', function(){
      document.location.href = './quiz/create';
    });
  },


  submitForm: function (evt) {
    evt.preventDefault();
    var $currentForm = $(this);
    var formData = $currentForm.serialize();
    $.ajax({
        url: evt.data.url,
        method: 'POST',
        dataType: 'json',
        data: formData
      }).done(function (response) {
        if (response['redirect'] != undefined) {
          document.location.href = response['redirect'];
        } else {
          $('.errors').show();        
           if (response.email != undefined) {
             $('#errorEmail').text(response.email);
             $('#inputEmail').addClass("is-invalid");
            }else {
              $('#errorEmail').text('');
              $('#inputEmail').removeClass("is-invalid");
              $('#inputEmail').addClass("is-valid");
           }
           if (response.password != undefined) {
            $('#errorPassword').text(response.password);
            $('#inputPassword').addClass("is-invalid");
          }else {
            $('#errorPassword').text('');
            $('#inputPassword').removeClass("is-invalid");
            $('#inputPassword').addClass("is-valid");
          }
          if (response.firstName != undefined) {
            $('#errorFirst').text(response.firstName);
            $('#inputFirst').addClass("is-invalid");
          }else {
            $('#errorFirst').text('');
            $('#inputFirst').removeClass("is-invalid");
            $('#inputFirst').addClass("is-valid");
           }
          if (response.lastName != undefined) {
            $('#errorLast').text(response.lastName);
            $('#inputLast').addClass("is-invalid");
          }else {
            $('#errorLast').text('');
            $('#inputLast').removeClass("is-invalid");
            $('#inputLast').addClass("is-valid");
           }
          if (response.passwordConfirm != undefined) {
            $('#errorPasswordConfirm').text(response.passwordConfirm);
            $('#inputConfirmPassword').addClass("is-invalid");
          }else {
            $('#errorPasswordConfirm').text('');
            $('#inputConfirmPassword').removeClass("is-invalid");
            $('#inputConfirmPassword').addClass("is-valid");
           }
        }
      })
      .fail(function (response) {
        console.log("ajax fail");
      })
  },
};

$(app.init);