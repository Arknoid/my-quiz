# Routes list

| Routes  |  Names | Controllers  |  Types | Methodes|
|--------|-------|-------------|----------|------|
|/       |main_home |  MAinController |GET   |home|
|/cgu       |  main_cgu |  MainController |GET   |cgu|
|/faq       |  main_faq |  MainController |GET   |faq|
|/signup   |user_signup  |UserController   | GET  |signup|
|/signin   |user_signin   |UserController   | GET  |signin|
|/user/logout   |user_logout   |UserController   |GET|logout|
|/user/signup   |user_signupSubmit   |UserController   |POST|signupSubmit|
|/user/signin   |user_signinSubmit   |UserController   |POST|signinSubmit|
|/user/profile   |user_profile   |UserController   |GET|profile|
|/quiz/[i:id]/   |quiz_show   |QuizController   |GET|show|
|/user/quiz/create   |quiz_create   |QuizController   |GET|create||
|/user/quiz/create/submit   |quiz_createSubmit   |QuizController   |POST|createSubmit|