<?php

namespace Oquiz;

//Import AltoRouter for Routing
use AltoRouter;

//Launcher is the Front Controller of the MVC model
class Launcher
{
    private $router;
    private $config;

    /**
     * __construct.
     */
    public function __construct()
    {
        //Retrieving config data
        $this->config = parse_ini_file(__DIR__.'/config.conf');
        //dump($this->config);exit;

        $this->router = new AltoRouter();
        //Configure the instantiated router
        $this->router->setBasePath($this->getConfig('BASE_PATH'));
        $this->defineRoutes();
    }

    /**
     * defineRoutes.
     */
    public function defineRoutes()
    {
        $this->router->map('GET', '/', 'MainController#home', 'main_home');
        $this->router->map('GET', '/cgu', 'MainController#cgu', 'main_cgu');
        $this->router->map('GET', '/faq', 'MainController#faq', 'main_faq');
        $this->router->map('GET', '/signup', 'UserController#signUp', 'user_signUp');
        $this->router->map('GET', '/signin', 'UserController#signIn', 'user_signIn');
        $this->router->map('POST', '/user/signup', 'UserController#signUpSubmit', 'user_signUpSubmit');
        $this->router->map('POST', '/user/signin', 'UserController#signInSubmit', 'user_signInSubmit');
        $this->router->map('GET', '/user/logout', 'UserController#logout', 'user_logout');
        $this->router->map('GET', '/user/profile', 'UserController#profile', 'user_profile');
        $this->router->map('GET', '/quiz/[i:id]', 'QuizController#showQuiz', 'quiz_showQuiz');
        $this->router->map('GET|POST', '/quiz/[i:id]/submit', 'QuizController#submitQuiz', 'quiz_submitQuiz');
        $this->router->map('GET', '/user/quiz/create', 'QuizController#create', 'quiz_create');
        $this->router->map('POST', '/user/quiz/create/submit', 'QuizController#createSubmit', 'quiz_createSubmit');
    }

    /**
     * run.
     */
    public function run()
    {
        $match = $this->router->match();
        //dump($match);exit;
        //DISPATCH for dynamic controllers and methodes instantiation  --
        if ($match !== false) {
            $dispatcherInfos = $match['target'];
            $controllerAndMethod = explode('#', $dispatcherInfos);
            $controllerName = $controllerAndMethod[0];
            $methodName = $controllerAndMethod[1];
            $controllerName = 'Oquiz\Controllers\\'.$controllerName;

            //Dynamic instantiation of the controller
            $controller = new $controllerName($this);
            //Dynamic call of the method
            $controller->$methodName($match['params']);
        } else {
            \Oquiz\Controllers\CoreController::sendHttpError(404, 'Error - 404');
        }
    }

    //GETTERS

    /**
     * getRouter.
     *
     * @return AltoRouter
     */
    public function getRouter(): AltoRouter
    {
        return $this->router;
    }

    //Returns a configuration of the config file

    /**
     * getConfig.
     *
     * @param string $param
     */
    public function getConfig(string $param)
    {
        if (array_key_exists($param, $this->config)) {
            return $this->config[$param];
        }

        return false;
    }
}
