<?php

namespace Oquiz\Models;

use Oquiz\Utils\Database;
use PDO;

class QuizModel
{
    private $id;
    private $title;
    private $description;
    private $id_author;
    // Inner join variables with a alias
    private $first_name_author;
    private $last_name_author;

    const TABLE_NAME = 'quizzes';

    /**
     * findAll.
     */
    public static function findAll(): array
    {
        $sql = '
          SELECT '.self::TABLE_NAME.'.id ,title , description ,id_author,users.first_name as first_name_author , users.last_name as last_name_author
          FROM '.self::TABLE_NAME.'
          INNER JOIN users ON users.id = '.self::TABLE_NAME.'.id_author
        ';
        $pdo = Database::getPDO();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->execute();

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    /**
     * findAllByAuthorId.
     *
     * @param mixed $authorId
     */
    public static function findAllByAuthorId($authorId): array
    {
        $sql = '
          SELECT '.self::TABLE_NAME.'.id ,title , description ,id_author,users.first_name as first_name_author , users.last_name as last_name_author
          FROM '.self::TABLE_NAME.'
          INNER JOIN users ON users.id = '.self::TABLE_NAME.'.id_author
          WHERE '.self::TABLE_NAME.'.id_author = :id
        ';
        $pdo = Database::getPDO();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindValue(':id', $authorId, PDO::PARAM_INT);
        $pdoStatement->execute();

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    /**
     * find.
     *
     * @param int $quizId
     */
    public static function find(int $quizId): QuizModel
    {
        $sql = '
          SELECT '.self::TABLE_NAME.'.id ,title , description ,id_author,users.first_name as first_name_author , users.last_name as last_name_author
          FROM '.self::TABLE_NAME.'
          INNER JOIN users ON users.id = '.self::TABLE_NAME.'.id_author
          WHERE '.self::TABLE_NAME.'.id = :id
        ';
        $pdo = Database::getPDO();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindValue(':id', $quizId, PDO::PARAM_INT);
        $pdoStatement->execute();

        return $pdoStatement->fetchObject(self::class);
    }

    /**
     * insert.
     */
    public function insert(): bool
    {
        $sql = '
          INSERT INTO '.self::TABLE_NAME.' (`title`,`description`, `id_author`)
          VALUES (:title, :description, :idAuthor)
      ';
        $pdoStatement = Database::getPDO()->prepare($sql);
        $pdoStatement->bindValue(':title', $this->title, PDO::PARAM_STR);
        $pdoStatement->bindValue(':description', $this->description, PDO::PARAM_STR);
        $pdoStatement->bindValue(':idAuthor', $this->id_author, PDO::PARAM_INT);
        $pdoStatement->execute();
        $affectedRows = $pdoStatement->rowCount();
        if ($affectedRows > 0) {
            $this->id = Database::getPDO()->lastInsertId();

            return true;
        }

        return false;
    }

    // GETTERS AND SETTERS

    /**
     * Get the value of title.
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title.
     *
     * @return self
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set the value of description.
     *
     * @return self
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of id_author.
     */
    public function getIdAuthor(): int
    {
        return $this->id_author;
    }

    /**
     * Set the value of id_author.
     *
     * @return self
     */
    public function setIdAuthor(int $id_author)
    {
        $this->id_author = $id_author;

        return $this;
    }

    /**
     * Get the value of first_name_author.
     */
    public function getFirstNameAuthor(): string
    {
        return $this->first_name_author;
    }

    /**
     * Set the value of first_name_author.
     *
     * @return self
     */
    public function setFirstNameAuthor(string $first_name_author)
    {
        $this->first_name_author = $first_name_author;

        return $this;
    }

    /**
     * Get the value of last_name_author.
     */
    public function getLastNameAuthor(): string
    {
        return $this->last_name_author;
    }

    /**
     * Set the value of last_name_author.
     *
     * @return self
     */
    public function setLastNameAuthor(string $last_name_author)
    {
        $this->last_name_author = $last_name_author;

        return $this;
    }

    /**
     * Get the value of id.
     */
    public function getId(): int
    {
        return $this->id;
    }
}
