<?php

namespace Oquiz\Models;

use Oquiz\Utils\Database;
use PDO;

class QuestionModel
{
    private $id_question;
    private $id_quiz;
    private $question;
    private $prop1;
    private $prop2;
    private $prop3;
    private $prop4;
    private $anecdote;
    private $wiki;
    // Join variables :
    private $name_level;
    private $id_level;

    private $propsList;
    private $levelColor;

    // 1 = good; 2 = bad; 0= empty
    private $userAnswer = 0;
    private $cardBackgroundColor;

    const TABLE_NAME = 'questions';

    const BADGE_COLOR_GREEN = 'badge-success';
    const BADGE_COLOR_ORANGE = 'badge-warning';
    const BADGE_COLOR_RED = 'badge-danger';

    const CARD_COLOR_SUCCESS = 'alert-success';
    const CARD_COLOR_ERRORS = 'alert-danger';
    const CARD_COLOR_EMPTY = '';

    /**
     * __construct.
     */
    public function __construct()
    {
        $this->propsList = [
                            ['id' => 1, 'prop' => $this->prop1, 'checked' => false],
                            ['id' => 2, 'prop' => $this->prop2, 'checked' => false],
                            ['id' => 3, 'prop' => $this->prop3, 'checked' => false],
                            ['id' => 4, 'prop' => $this->prop4, 'checked' => false],
                           ];
        $this->setLevelColor();
        $this->setCardColor();
    }

    /**
     * findAllByQuizId.
     *
     * @param int $quizId
     */
    public static function findAllByQuizId(int $quizId): array
    {
        $sql = '
      SELECT *,levels.name as name_level, '.self::TABLE_NAME.'.id as id_question
      FROM '.self::TABLE_NAME.'
      INNER JOIN quizzes ON quizzes.id = '.self::TABLE_NAME.'.id_quiz
      INNER JOIN levels ON levels.id = '.self::TABLE_NAME.'.id_level
      WHERE '.self::TABLE_NAME.'.id_quiz = :id
      ';
        $pdo = Database::getPDO();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindValue(':id', $quizId, PDO::PARAM_INT);
        $pdoStatement->execute();

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    /**
     * findGoodAnswerByID.
     *
     * @param mixed $id
     */
    public function findGoodAnswerByID($id)
    {
        $sql = '
      SELECT prop1
      FROM '.static::TABLE_NAME.'
      WHERE id = :id
      ';
        $pdo = Database::getPDO();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindValue(':id', $id, PDO::PARAM_INT);
        $pdoStatement->execute();
        if ($pdoStatement === false) {
            throw new \Exception('Event#'.$id.'not found');
        }

        return $pdoStatement->fetchColumn();
    }

    /**
     * insert.
     */
    public function insert(): bool
    {
        $sql = '
          INSERT INTO '.self::TABLE_NAME.' (`id_quiz`,`question`, `prop1`, `prop2`,`prop3`,`prop4`,`id_level`,`anecdote`,`wiki`)
          VALUES (:idQuiz, :question, :prop1, :prop2, :prop3, :prop4, :idLevel ,:anectode, :wiki)
      ';
        $pdoStatement = Database::getPDO()->prepare($sql);
        $pdoStatement->bindValue(':idQuiz', $this->id_quiz, PDO::PARAM_INT);
        $pdoStatement->bindValue(':question', $this->question, PDO::PARAM_STR);
        $pdoStatement->bindValue(':prop1', $this->prop1, PDO::PARAM_STR);
        $pdoStatement->bindValue(':prop2', $this->prop2, PDO::PARAM_STR);
        $pdoStatement->bindValue(':prop3', $this->prop3, PDO::PARAM_STR);
        $pdoStatement->bindValue(':prop4', $this->prop4, PDO::PARAM_STR);
        $pdoStatement->bindValue(':idLevel', $this->id_level, PDO::PARAM_INT);
        $pdoStatement->bindValue(':anectode', $this->anecdote, PDO::PARAM_STR);
        $pdoStatement->bindValue(':wiki', $this->wiki, PDO::PARAM_STR);
        $pdoStatement->execute();
        $affectedRows = $pdoStatement->rowCount();
        if ($affectedRows > 0) {
            $this->id = Database::getPDO()->lastInsertId();

            return true;
        }

        return false;
    }

    /**
     * setLevelColor.
     */
    private function setLevelColor()
    {
        switch ($this->id_level) {
        case 1:
        $this->levelColor = self::BADGE_COLOR_GREEN;
        break;
        case 2:
        $this->levelColor = self::BADGE_COLOR_ORANGE;
        break;
        case 3:
        $this->levelColor = self::BADGE_COLOR_RED;
        break;
      }
    }

    /**
     * setCardColor.
     */
    private function setCardColor()
    {
        switch ($this->userAnswer) {
        case 0:
        $this->cardBackgroundColor = self::CARD_COLOR_EMPTY;
        break;
        case 1:
        $this->cardBackgroundColor = self::CARD_COLOR_SUCCESS;
        break;
        case 2:
        $this->cardBackgroundColor = self::CARD_COLOR_ERRORS;
        break;
      }
    }

    /**
     * shufflePropsList.
     */
    public function shufflePropsList()
    {
        shuffle($this->propsList);
        $this->setNewIdsPropsList();
    }

    /**
     * setNewIdsPropsList.
     */
    private function setNewIdsPropsList()
    {
        $newId = 1;
        foreach ($this->propsList as $key => $value) {
            $this->propsList[$key]['id'] = $newId++;
        }
    }

    // GETTERS AND SETTERS

    /**
     * setCheckedPropsListById.
     *
     * @param int $id
     */
    public function setCheckedPropsListById(int $id): bool
    {
        foreach ($this->propsList as $key => $value) {
            if ($value['id'] == $id) {
                $this->propsList[$key]['checked'] = true;

                return true;
            }
        }

        return false;
    }

    /**
     * Get the value of id_quiz.
     */
    public function getIdQuiz(): int
    {
        return $this->id_quiz;
    }

    /**
     * Set the value of id_quiz.
     *
     * @return self
     */
    public function setIdQuiz(int $id_quiz)
    {
        $this->id_quiz = $id_quiz;

        return $this;
    }

    /**
     * Get the value of question.
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * Set the value of question.
     *
     * @return self
     */
    public function setQuestion(string $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get the value of prop1.
     */
    public function getProp1(): string
    {
        return $this->prop1;
    }

    /**
     * Set the value of prop1.
     *
     * @return self
     */
    public function setProp1(string $prop1)
    {
        $this->prop1 = $prop1;

        return $this;
    }

    /**
     * Get the value of prop2.
     */
    public function getProp2(): string
    {
        return $this->prop2;
    }

    /**
     * Set the value of prop2.
     *
     * @return self
     */
    public function setProp2(string $prop2)
    {
        $this->prop2 = $prop2;

        return $this;
    }

    /**
     * Get the value of prop3.
     */
    public function getProp3(): string
    {
        return $this->prop3;
    }

    /**
     * Set the value of prop3.
     *
     * @return self
     */
    public function setProp3(string $prop3)
    {
        $this->prop3 = $prop3;

        return $this;
    }

    /**
     * Get the value of prop4.
     */
    public function getProp4(): string
    {
        return $this->prop4;
    }

    /**
     * Set the value of prop4.
     *
     * @return self
     */
    public function setProp4(string $prop4)
    {
        $this->prop4 = $prop4;

        return $this;
    }

    /**
     * Get the value of anecdote.
     */
    public function getAnecdote(): string
    {
        return $this->anecdote;
    }

    /**
     * Set the value of anecdote.
     *
     * @return self
     */
    public function setAnecdote(string $anecdote)
    {
        $this->anecdote = $anecdote;

        return $this;
    }

    /**
     * Get the value of wiki.
     */
    public function getWiki(): string
    {
        return $this->wiki;
    }

    /**
     * Set the value of wiki.
     *
     * @return self
     */
    public function setWiki(string $wiki)
    {
        $this->wiki = $wiki;

        return $this;
    }

    /**
     * Get the value of propsList.
     */
    public function getPropsList(): array
    {
        return $this->propsList;
    }

    /**
     * Set the value of propsList.
     *
     * @return self
     */
    public function setPropsList($propsList)
    {
        $this->propsList = $propsList;

        return $this;
    }

    /**
     * Get the value of level.
     */
    public function getNameLevel(): string
    {
        return $this->name_level;
    }

    /**
     * Get the value of levelColor.
     */
    public function getLevelColor(): string
    {
        return $this->levelColor;
    }

    /**
     * Get the value of id.
     */
    public function getIdQuestion(): int
    {
        return $this->id_question;
    }

    /**
     * Get the value of userAnswer.
     */
    public function getUserAnswer(): int
    {
        return $this->userAnswer;
    }

    /**
     * Set the value of userAnswer.
     *
     * @return self
     */
    public function setUserAnswer(int $userAnswer)
    {
        $this->userAnswer = $userAnswer;
        $this->setCardColor();

        return $this;
    }

    /**
     * Get the value of cardBackgroundColor.
     */
    public function getCardBackgroundColor(): string
    {
        return $this->cardBackgroundColor;
    }

    /**
     * Get the value of id_level.
     */
    public function getIdLevel()
    {
        return $this->id_level;
    }

    /**
     * Set the value of id_level.
     *
     * @return self
     */
    public function setIdLevel(int $id_level)
    {
        $this->id_level = $id_level;

        return $this;
    }
}
