<?php

namespace Oquiz\Models;

use Oquiz\Utils\Database;
use PDO;

class UserModel
{
    private $id;
    private $first_name;
    private $last_name;
    private $email;
    private $password;

    const TABLE_NAME = 'users';

    /**
     * findByEmail.
     *
     * @param string $email
     */
    public static function findByEmail(string $email)
    {
        $sql = '
          SELECT *
          FROM '.self::TABLE_NAME.'
          WHERE email = :email
      ';
        $pdoStatement = Database::getPDO()->prepare($sql);
        $pdoStatement->bindValue(':email', $email, PDO::PARAM_STR);
        $pdoStatement->execute();

        return $pdoStatement->fetchObject(self::class);
    }

    /**
     * insert.
     */
    public function insert(): bool
    {
        $sql = '
          INSERT INTO '.self::TABLE_NAME.' (`first_name`, `last_name`, `email`, `password`)
          VALUES (:firstName, :lastName, :email, :password)
      ';
        $pdoStatement = Database::getPDO()->prepare($sql);
        $pdoStatement->bindValue(':firstName', $this->first_name, PDO::PARAM_STR);
        $pdoStatement->bindValue(':lastName', $this->last_name, PDO::PARAM_STR);
        $pdoStatement->bindValue(':email', $this->email, PDO::PARAM_STR);
        $pdoStatement->bindValue(':password', $this->password, PDO::PARAM_STR);
        $pdoStatement->execute();
        $affectedRows = $pdoStatement->rowCount();
        if ($affectedRows > 0) {
            $this->id = Database::getPDO()->lastInsertId();

            return true;
        }

        return false;
    }

    // GETTERS AND SETTERS

    /**
     * Get the value of first_name.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the value of first_name.
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * Set the value of first_name.
     *
     * @return self
     */
    public function setFirstName(string $first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * Get the value of last_name.
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * Set the value of last_name.
     *
     * @return self
     */
    public function setLastName(string $last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Get the value of email.
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set the value of email.
     *
     * @return self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password.
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Set the value of password.
     *
     * @return self
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }
}
