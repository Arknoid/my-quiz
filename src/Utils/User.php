<?php

namespace Oquiz\Utils;

abstract class User
{
    /**
     * isConnected.
     */
    public static function isConnected(): bool
    {
        return
            array_key_exists('connected-user', $_SESSION) &&
            is_object($_SESSION['connected-user']) &&
            $_SESSION['connected-user-ip'] == $_SERVER['REMOTE_ADDR']
        ;
    }

    /**
     * getConnectedUser.
     */
    public static function getConnectedUser()
    {
        return $_SESSION['connected-user'];
    }

    /**
     * connect.
     *
     * @param mixed $userModel
     */
    public static function connect($userModel): bool
    {
        $_SESSION['connected-user'] = $userModel;
        $_SESSION['connected-user-ip'] = $_SERVER['REMOTE_ADDR'];

        return true;
    }

    /**
     * disconnect.
     */
    public static function disconnect()
    {
        if (self::isConnected()) {
            unset($_SESSION['connected-user']);
            unset($_SESSION['connected-user-ip']);
        }
    }
}
