<?php

namespace Oquiz\Controllers;

use Oquiz\Models\QuizModel;
use Oquiz\Models\QuestionModel;
use Oquiz\Utils\User;

class QuizController extends CoreController
{
    // Constant Bootstrap for the background color of the display
    const DISPLAY_COLOR_NEWGAME = 'alert-primary';
    const DISPLAY_COLOR_DONE = 'alert-success';

    // Text to display in the view
    const DISPLAY_MESSAGE_NEWGAME = 'Nouveau jeux : Répondez au maximum de questions avant de valider';

    /**
     * showQuiz.
     *
     * @param mixed $urlparams
     */
    public function showQuiz($urlparams)
    {
        $quiz = QuizModel::find($urlparams['id']);
        $questionsById = QuestionModel :: findAllByQuizId($urlparams['id']);
        // Shuffle questions list
        foreach ($questionsById as $key => $question) {
            $question->shufflePropsList();
        }

        $dataToViews = [
            'colorDisplay' => self::DISPLAY_COLOR_NEWGAME,
            'result' => false,
            'textToDisplay' => self::DISPLAY_MESSAGE_NEWGAME,
            'quiz' => $quiz,
            'listOfQuestions' => $questionsById,
            'numberOfQuestions' => count($questionsById),
        ];

        $userConnected = User::isConnected() ? User::getConnectedUser() : false;
        if ($userConnected) {
            $this->show('quiz/formQuiz', $dataToViews);
        } else {
            $this->show('quiz/quiz', $dataToViews);
        }
    }

    /**
     * submitQuiz.
     *
     * @param mixed $urlparams
     */
    public function submitQuiz($urlparams)
    {
        $score = 0;
        $userProps = [];

        if (empty($_POST) === false) {
            foreach ($_POST as $id => $props) {
                $propsExplode = explode('#', $props);
                $prop = $propsExplode[0];
                //Retrieve the position of the form radio
                $propId = $propsExplode[1];

                $goodAnswer = QuestionModel :: findGoodAnswerByID($id);
                if ($goodAnswer === $prop) {
                    array_push($userProps, ['id' => $id, 'isTrue' => true, 'idList' => $propId]);
                    //increment score
                    ++$score;
                } else {
                    array_push($userProps, ['id' => $id, 'isTrue' => false, 'idList' => $propId]);
                }
            }
        }

        $quiz = QuizModel::find($urlparams['id']);
        $questionsById = QuestionModel :: findAllByQuizId($urlparams['id']);
        //for each question:
        foreach ($questionsById as $key => $question) {
            //And for each answer we get the id and the props and we assign
            foreach ($userProps as $key => $value) {
                if ($value['id'] === $question->getIdQuestion()) {
                    // Stock the choice of the user for the checkbox
                    $question->setCheckedPropsListById($value['idList']);
                    if ($value['isTrue']) {
                        $question->setUserAnswer(1);
                    } else {
                        $question->setUserAnswer(2);
                    }
                }
            }
        }

        $dataToViews = [
            'result' => true,
            'colorDisplay' => self::DISPLAY_COLOR_DONE,
            'textToDisplay' => 'Votre score : '.$score.'/'.count($questionsById),
            'quiz' => $quiz,
            'listOfQuestions' => $questionsById,
            'numberOfQuestions' => count($questionsById),
        ];
        $this->show('quiz/formQuiz', $dataToViews);
    }

    /**
     * create.
     */
    public function create()
    {
        //For the moment we can create only quizzes of 10 questions
        $dataToViews = [
        'numberOfQuestions' => 10,
    ];
        $this->show('quiz/create', $dataToViews);
    }

    /**
     * createSubmit.
     */
    public function createSubmit()
    {
        // Part not finished !!!

        $quizName;
        $quizDescrib;

        $questions = [];
        $errorList = [];
        $questions = [];
        // formulaire soumis
        if (!empty($_POST)) {
            // var_dump($_POST);
            // exit;

            // Je récupère les données
            foreach ($_POST as $key => $value) {
                $content['value'] = trim(isset($value) ? $value : '');
                $explodeValue = explode('#', $key);
                $content['name'] = $explodeValue[1];
                $id = intval($explodeValue[0]);

                if ($content['name'] === 'quizName') {
                    $quizName = $value;
                } elseif ($content['name'] === 'quizDescrib') {
                    $quizDescrib = $value;
                // $questions[$id] += $content['value'];
                } else {
                    switch ($content['name']) {
                      case 'question':
                      $questions[$id]['question'] = $value;
                        break;
                      case 'difficulty':
                      $questions[$id]['difficulty'] = $value;
                        break;
                      case 'answer1':
                      $questions[$id]['answer1'] = $value;
                        break;
                      case 'answer2':
                      $questions[$id]['answer2'] = $value;
                        break;
                      case 'answer3':
                      $questions[$id]['answer3'] = $value;
                        break;
                      case 'answer4':
                      $questions[$id]['answer4'] = $value;
                        break;
                      case 'anectdote':
                      $questions[$id]['anectdote'] = $value;
                        break;
                      case 'wiki':
                      $questions[$id]['wiki'] = $value;
                        break;
                  }
                }
            }
            // }
            $user = User::getConnectedUser();
            $userId = $user->getId();
            //On insert en Db le nouveau Quiz
            $newQuizModel = new QuizModel();
            $newQuizModel->setTitle($quizName);
            $newQuizModel->setDescription($quizDescrib);
            $newQuizModel->setIdAuthor($userId);
            $newQuizModel->insert();
            foreach ($questions as $key => $value) {
                $newQuestionModel = new QuestionModel();
                $newQuestionModel->setQuestion($value['question']);
                $newQuestionModel->setIdLevel(intval($value['difficulty']));
                $newQuestionModel->setIdQuiz($newQuizModel->getId());
                $newQuestionModel->setProp1($value['answer1']);
                $newQuestionModel->setProp2($value['answer2']);
                $newQuestionModel->setProp3($value['answer3']);
                $newQuestionModel->setProp4($value['answer4']);
                $newQuestionModel->setAnecdote($value['anectdote']);
                $newQuestionModel->setWiki($value['wiki']);
                // On insert en DB la Questions
                $newQuestionModel->insert();
            }

            // // On envoie un JSON avec l'url du home pour la redirection
            $this->sendJson(array('redirect' => $this->router->generate('user_profile')));
        }
        $this->sendJson($errorList);
    }
}
