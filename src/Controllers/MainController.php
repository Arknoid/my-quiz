<?php

namespace Oquiz\Controllers;

use Oquiz\Models\QuizModel;

class MainController extends CoreController
{
    /**
     * home.
     */
    public function home()
    {
        $quizList = QuizModel::findAll();

        $dataToViews = [
            'listOfQuizz' => $quizList,
        ];

        $this->show('main/home', $dataToViews);
    }

    /**
     * cgu.
     */
    public function cgu()
    {
        $this->show('main/cgu');
    }

    /**
     * faq.
     */
    public function faq()
    {
        $this->show('main/faq');
    }
}
