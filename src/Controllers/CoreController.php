<?php

namespace Oquiz\Controllers;

//Import Pantes-engine  from composer for View
use League\Plates\Engine as Plates;
use Oquiz\Launcher;
use Oquiz\Utils\User;

abstract class CoreController
{
    //Makes templateEngine(Plates) and router(AltoRouter) available to children
    protected $templateEngine;
    protected $router;

    public function __construct(Launcher $app)
    {
        //Assignment of AltoRouter to $router
        $this->router = $app->getRouter();

        //Assignment of Plates to $templateEngine
        $this->templateEngine = new Plates(__DIR__.'/../Views');

        //Allow data to all views
        $this->templateEngine->addData([
            'router' => $this->router,
            'basePath' => $app->getConfig('BASE_PATH'),
            'connectedUser' => User::isConnected() ? User::getConnectedUser() : false,
        ]);
    }

    /**
     * show
     * Method for displaying the view for a page.
     *
     * @param string $templateName
     * @param mixed array
     */
    protected function show(string $templateName, array $dataToViews = [])
    {
        $viewFolder = '';
        echo $this->templateEngine->render($viewFolder.$templateName, $dataToViews);
    }

    /**
     * sendJson
     * Method for answer formated data in Json to Ajax request.
     *
     * @param mixed $data
     */
    protected static function sendJson($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * sendHttpError
     * Method for generate HTTP header errors.
     *
     * @param mixed $errorCode
     * @param mixed $htmlContent
     */
    public static function sendHttpError($errorCode, $htmlContent = '')
    {
        // Error 404 not found
        if ($errorCode == 404) {
            header('HTTP/1.0 404 Not Found');
            echo $htmlContent;
            exit;
        }
        // Error 403 Forbidden
        elseif ($errorCode == 403) {
            header('HTTP/1.0 403 Forbidden');
            echo $htmlContent;
            exit;
        }
    }

    /**
     * redirect
     * Method for redirecting in PHP to a URL.
     *
     * @param string $url
     */
    public function redirect(string $url)
    {
        header('Location: '.$url);
        exit;
    }

    /**
     * redirectToRoute
     * Method for redirecting to the URL of the provided route as an argument.
     *
     * @param string $routeName
     */
    public function redirectToRoute(string $routeName)
    {
        $this->redirect($this->router->generate($routeName));
    }
}
