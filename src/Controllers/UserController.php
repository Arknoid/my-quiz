<?php

namespace Oquiz\Controllers;

use Oquiz\Models\UserModel;
use Oquiz\Utils\User;
use Oquiz\Models\QuizModel;

class UserController extends CoreController
{
    /**
     * signUp.
     */
    public function signUp()
    {
        $this->show('user/signup');
    }

    /**
     * signIn.
     */
    public function signIn()
    {
        $this->show('user/signin');
    }

    /**
     * signUpSubmit.
     */
    public function signUpSubmit()
    {
        // Array containing all errors
        $errorList = [
        ];
        // formulaire soumis
        if (!empty($_POST)) {
            // Je récupère les données
            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $password = isset($_POST['password']) ? $_POST['password'] : '';
            $passwordConfirm = isset($_POST['passwordConfirm']) ? $_POST['passwordConfirm'] : '';
            $firstName = isset($_POST['firstName']) ? $_POST['firstName'] : '';
            $lastName = isset($_POST['lastName']) ? $_POST['lastName'] : '';
            // Je traite les données si besoin
            $email = trim($email);
            $password = trim($password);
            $passwordConfirm = trim($passwordConfirm);
            $lastName = trim($lastName);
            $firstName = trim($firstName);
            // Je valide les données => je cherche les erreurs
            if (empty($email)) {
                $errorList['email'] = 'Email vide';
            } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $errorList['email'] = 'Email non valid';
            }
            if (empty($password)) {
                $errorList['password'] = 'Mot de passe vide';
            }

            if (empty($firstName)) {
                $errorList['firstName'] = 'le prenom est vide';
            }
            if (empty($lastName)) {
                $errorList['lastName'] = 'le nom est vide';
            }
            if (empty($passwordConfirm)) {
                $errorList['passwordConfirm'] = 'Confirmation de mot de passe vide';
            }
            if ($password != $passwordConfirm) {
                $errorList['passwordConfirm'] = 'Les 2 mots de passe sont différents';
            }
            if (strlen($password) < 8) {
                $errorList['password'] = 'Le mot de passe doit faire au moins 8 caractères';
            }
            // Si tout est ok (aucune erreur)

            if (empty($errorList)) {
                // On vérifie la présence de l'adresse email en DB
                $userModel = UserModel::findByEmail($email);
                // Si l'email existe
                if ($userModel !== false) {
                    // Ajouter un message d'erreur
                    $errorList['email'] = 'Un compte avec cette adresse email existe déjà';
                }
                // Sinon
                else {
                    // Je hash/encode le mot de passe
                    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                    // On insert en DB
                    $newUserModel = new UserModel();
                    $newUserModel->setEmail($email);
                    $newUserModel->setPassword($hashedPassword);
                    $newUserModel->setLastName($lastName);
                    $newUserModel->setFirstName($firstName);
                    $newUserModel->insert();
                    // Sauvegarde en Session de l'user
                    User::connect($newUserModel);
                    // On envoie un JSON avec l'url du home pour la redirection
                    $this->sendJson(array('redirect' => $this->router->generate('main_home')));
                }
            }
            $this->sendJson($errorList);
        }
    }

    /**
     * signInSubmit.
     */
    public function signInSubmit()
    {
        // Tableau contenant toutes les erreurs
        $errorList = [];

        // Je récupère les données
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';

        // Je traite les données si besoin
        $email = trim($email);
        $password = trim($password);

        // Je valide les données => je cherche les erreurs
        if (empty($email)) {
            $errorList['email'] = 'Email vide';
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $errorList['email'] = 'Email incorrect';
        }
        if (empty($password)) {
            $errorList['password'] = 'Mot de passe vide';
        }
        if (strlen($password) < 8) {
            $errorList['password'] = 'Le mot de passe doit faire au moins 8 caractères';
        }

        // Si tout est ok (aucune erreur)
        if (empty($errorList)) {
            // On va cherche le user pour l'email fourni
            $userModel = UserModel::findByEmail($email);

            // Si l'email existe
            if ($userModel !== false) {
                // Alors, on va tester le password
                if (password_verify($password, $userModel->getPassword())) {
                    // Je connecte l'utilisateur, peut importe comme cela est fait
                    User::connect($userModel);

                    $this->sendJson(array('redirect' => $this->router->generate('main_home')));
                } else {
                    $errorList['email'] = 'Identifiants/mot de passe non reconnus';
                }
            }
            // Sinon
            else {
                $errorList['email'] = 'Identifiants/mot de passe non reconnus';
            }
        }

        $this->sendJson($errorList);
    }

    /**
     * logout.
     */
    public function logout()
    {
        User::disconnect();
        $this->redirectToRoute('main_home');
    }

    /**
     * profile.
     */
    public function profile()
    {
        $user = User::isConnected() ? User::getConnectedUser() : false;
        $quizList = QuizModel::findAllByAuthorId($user->getId());
        $dataToViews = [
          'listOfQuizz' => $quizList,
      ];

        if ($user != false) {
            $this->show('user/profile', $dataToViews);
        } else {
            self::sendHttpError(403, "Vous n'avez pas acces a cette page ...");
        }
    }
}
