<?php $this->layout('layout', ['title' => 'Quiz visiteur']); ?>
<!--Insert a Partial for Header and assign data-->
<?= $this->insert('partials/quizHeader', [
                                          'title' => $quiz->getTitle(),
                                          'description' => $quiz->getDescription(),
                                          'numberOfQuestions' => $numberOfQuestions,
                                          'author' => $quiz->getFirstNameAuthor().' '.$quiz->getLastNameAuthor(),
                                          ]); ?>

  <div class="row m-3">
    <?php foreach ($listOfQuestions as $currentQuestion) :?>
    <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
      <div class="card shadow ">
        <div class="card-header">
          <div class="float-right ml-4 badge <?= $this->e($currentQuestion->getLevelColor()); ?>">
            <?= $this->e($currentQuestion->getNameLevel()); ?>
          </div>
          <?= $this->e($currentQuestion->getQuestion()); ?>
        </div>
        <div class="card-body">
          <ol>
            <?php foreach ($currentQuestion->getPropsList() as $propsList) :?>
            <li>
              <?=$propsList['prop']; ?>
            </li>
            <?php endforeach; ?>
          </ol>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>