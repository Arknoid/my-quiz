<?php $this->layout('layout', ['title' => 'Quiz invité']); ?>
<?= $this->insert('partials/quizHeader', [
                                          'title' => $quiz->getTitle(),
                                          'description' => $quiz->getDescription(),
                                          'numberOfQuestions' => $numberOfQuestions,
                                          'author' => $quiz->getFirstNameAuthor().' '.$quiz->getLastNameAuthor(),
                                          ]); ?>

<div class="alert <?=$this->e($colorDisplay); ?> m-3" id="quizConsole" role="alert">
  <?= $this->e($textToDisplay); ?>
<?php if ($result) :?>
<a class="d-block" href="<?= $router->generate('quiz_showQuiz', ['id' => $quiz->getId()]); ?>">Rejouer</a>
<?php endif; ?>
</div>
<div class="row m-3">
    <?php foreach ($listOfQuestions as $currentQuestion) :?>
      <form id="formQuiz" method="POST" action="<?=$router->generate('quiz_submitQuiz', ['id' => $this->e($quiz->getId())]); ?>" class="col-12 row m-3">
      <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
          <div class="card shadow ">
              <div class="card-header <?= $this->e($currentQuestion->getCardBackgroundColor()); ?> ">
                  <div class="float-right ml-4 badge <?= $this->e($currentQuestion->getLevelColor()); ?>"><?= $this->e($currentQuestion->getNameLevel()); ?></div>
                  <?= $this->e($currentQuestion->getQuestion()); ?>
              </div>
              
              <div class="card-body">
                  <ol>
                      <?php
                      foreach ($currentQuestion->getPropsList() as $propsList) :?>
                          <li class="form-check" >
                            <input class="form-check-input" 
                              <?=$checked = $propsList['checked'] ? 'checked=\"checked\" ' : ''; ?>
                              type="radio" name="<?= $currentQuestion->getIdQuestion(); ?>" value="<?=$propsList['prop'].'#'.$propsList['id']; ?>">
                            <label for =<?=$propsList['prop']; ?>><?=$propsList['prop']; ?></label>
                          </li>
                      <?php endforeach; ?>
                  </ol>
              </div>
                <div class="card-footer <?= $display = ($this->e($currentQuestion->getUserAnswer() != 0)) ? '' : 'hide'; ?>">
                  <?= $this->e($currentQuestion->getAnecdote()); ?>
                    <div class="mt-2">
                        <a href="https://fr.wikipedia.org/wiki/<?= $this->e($currentQuestion->getWiki()); ?>">Wikipedia(<?= $this->e($currentQuestion->getWiki()); ?>)</a>
                    </div>
                </div>
          </div>
      </div>
    <?php endforeach; ?>
    <?php if ($result) :?>
    <a class="col-12 btn btn-success mt-5" href="<?= $router->generate('quiz_showQuiz', ['id' => $quiz->getId()]); ?>" role="button">Rejouer</a>
    <?php else: ?>
      <button type="submit" class="col-12 btn btn-primary mt-5">OK</button>
    <?php endif; ?>
      </form>
</div>