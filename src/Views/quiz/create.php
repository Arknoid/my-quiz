<?php $this->layout('layout', ['title' => 'Créateur de Quiz']); ?>

<div class="jumbotron jumbotron-fluid shadow rounded mt-4">
  <div class="container">
    <h1 class="display-4 text-center">Créateur de quiz </h1>
  </div>
</div>

<form action="POST" id="formCreate" class="row col-12">
  
  <div class="card shadow col-12 pt-3">
    <div class="card-body">
      <label for="inputQuizName" class="font-weight-bold">Titre :</label>
      <input type="text" name='<?=$connectedUser->getId().'#'; ?>quizName' class="form-control" id="inputQuizName" aria-describedby="Quiz name" placeholder="Entrez le titre du quiz"></input>
      <label for="inputQuizDescrib" class="font-weight-bold">Description :</label>
      <textarea type="text" name='<?=$connectedUser->getId().'#'; ?>quizDescrib' class="form-control" id="inputQuizDescrib" aria-describedby="Quiz describ" placeholder="Entrez une description"></textarea>
    </div>
  </div>
  <?php for ($number = 1; $number <= $numberOfQuestions; ++$number) :?>
  <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
    <div class="card shadow ">
      <div class="card-header">
        <div class="form-group">
          <label for="input<?=$number; ?>Question " class="font-weight-bold">Question <?=$number; ?></label>
          <textarea class="form-control" name='<?=$number.'#'; ?>question' id="input<?=$number; ?>Question" rows="3" placeholder="Entrez une question"></textarea>
        </div>
        <label for="inputDifficulty<?=$number; ?>" class="font-weight-bold">Difficulté</label>
        <select id="inputDifficulty<?=$number; ?>" name='<?=$number.'#'; ?>difficulty' class="form-control">
          <option value="1" selected= "selected">Débutant</option>
          <option value="2">Confirmé</option>
          <option value="3" >Expert</option>
        </select>
      </div>

      <div class="card-body">
        <h5>Réponses :</h5>
        <ol class="mt-4 pl-0">
          <li class="form-check pl-0">
            <label for="input<?=$number; ?>Answer1" class="font-weight-bold">Choix 1 :</label>
            <p class="text-success">Bonne réponse</p>
            <input type="text" name='<?=$number.'#'; ?>answer1' class="form-control" id="input<?=$number; ?>Answer1" aria-describedby="Answer1" placeholder="Entrez une réponse">
          </li>
          <li class="form-check pl-0">
            <label for="input<?=$number; ?>Answer2" class="font-weight-bold">Choix 2 :</label>
            <input type="text" name='<?=$number.'#'; ?>answer2' class="form-control" id="input<?=$number; ?>Answer2" aria-describedby="Answer2" placeholder="Entrez une réponse">
          </li>
          <li class="form-check pl-0">
            <label for="input<?=$number; ?>Answer3" class="font-weight-bold">Choix 3 :</label>
            <input type="text" name='<?=$number.'#'; ?>answer3' class="form-control" id="input<?=$number; ?>Answer3" aria-describedby="Answer3" placeholder="Entrez une réponse">
          </li>
          <li class="form-check pl-0">
            <label for="input<?=$number; ?>Answer4" class="font-weight-bold">Choix 4:</label>
            <input type="text" name='<?=$number.'#'; ?>answer4' class="form-control" id="input<?=$number; ?>Answer4" aria-describedby="Answer4" placeholder="Entrez une réponse">
          </li>
        </ol>
      </div>
      <div class="card-footer">
        <label for="anectdote<?=$number; ?>Input" class="font-weight-bold">Anecdote</label>
        <textarea class="form-control" name='<?=$number.'#'; ?>anectdote' id="anectdote<?=$number; ?>Input" rows="3" placeholder="Entrez une anecdote"></textarea>
        <div class="mt-2">
          <li class="form-check pl-0">
            <label for="wiki<?=$number; ?>Input" class="font-weight-bold">Mot-clef pour Wikipedia</label>
            <input type="text" class="form-control" name='<?=$number.'#'; ?>wiki' id="wiki<?=$number; ?>Input" aria-describedby="wiki" placeholder="Entrez un mot-clef">
          </li>
        </div>
      </div>
    </div>
  </div>
  <?php endfor; ?>
  <button type="submit" class="col-12 btn btn-primary mt-5">Créer</button>
</form>