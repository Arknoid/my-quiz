<?php $this->layout('layout', ['title' => 'Inscription']); ?>

<div class="jumbotron jumbotron-fluid shadow rounded m-5 ">
    <div class="container text-center">
        <h1 class="display-4">Inscription</h1>
    </div>
</div>
<div class="container d-flex justify-content-center">
    <form  class="mt-1 col-lg-6" id="formSignUp">
        <div class="form-group">
            <label for="inputEmail1 " class="font-weight-bold">Identifiant (adresse mail)</label>
            <input type="email"  name="email" class="form-control" id="inputEmail" aria-describedby="Identifiant" placeholder="Entez votre email">
            <div class="text-danger error" id="errorEmail"></div>
        </div>
        <div class="form-group">
            <label for="inputEmail1 " class="font-weight-bold">Prenom</label>
            <input type="text" name="firstName" class="form-control" id="inputFirst" aria-describedby="First name" placeholder="Entrez votre prenom">
            <div class="text-danger errors" id="errorFirst" ></div>
        </div>
        <div class="form-group">
            <label for="inputEmail1 " class="font-weight-bold">Nom</label>
            <input type="text" name="lastName" class="form-control" id="inputLast" aria-describedby="Last name" placeholder="Entrez votre nom">
            <div class="text-danger errors" id="errorLast"></div>
        </div>
        <div class="form-group">
            <label for="inputPassword " class="font-weight-bold">Mot de passe</label>
            <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Entrez le mot de passe">
            <div class="text-danger errors" id="errorPassword"></div>
        </div>
        <div class="form-group">
            <label for="inputConfirmPassword " class="font-weight-bold">Confirmer le mot de passe</label>
            <input type="password" name="passwordConfirm" class="form-control" id="inputConfirmPassword" placeholder="Confirmez le mot de passe">
            <div class="text-danger errors" id="errorConfirmPassword"></div>
        </div>
        <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Je m'inscrit</button>
            <a class="ml-5" href="<?=$router->generate('user_signIn'); ?>">Déja inscrit?</a>
        </div>
    </form>
</div>