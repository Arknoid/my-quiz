<?php $this->layout('layout', ['title' => 'Connection']); ?>

<div class="jumbotron jumbotron-fluid shadow rounded m-5 ">
    <div class="container text-center">
        <h1 class="display-4">Connection</h1>
    </div>
</div>
<div class="container d-flex justify-content-center">
    <form class="mt-1 col-lg-6" id="formSignIn">
        <div class="show alert alert-danger errors" role="alert">
            Identifiant ou mot de passe incorrect
        </div>
        <div class="form-group">
            <label for="inputEmail1 " class="font-weight-bold">Votre identifiant (adresse mail)</label>
            <input type="email" name="email" class="form-control" id="inputEmail" aria-describedby="Identifiant" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="inputPassword1" class="font-weight-bold">Mot de passe</label>
            <input type="password" name='password' class="form-control" placeholder="Mot de passe">
        </div>
        <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Je me connect</button>
            <a href="<?=$router->generate('user_signUp'); ?>">Pas encore inscrit?</a>
        </div>
    </form>
</div>