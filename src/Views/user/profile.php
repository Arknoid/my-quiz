<?php $this->layout('layout', ['title' => 'Oquiz']); ?>

<div class="jumbotron jumbotron-fluid shadow rounded m-4">
  <div class="container">
    <h1 class="display-5 ">Bienvenue
      <?= $connectedUser->getFirstName(); ?> sur ton profil</h1>
    <p class="lead">Ici tu peu consulter la liste des quizs que tu a crée</p>
  </div>
</div>

<div class="row m-3">
  <?php foreach ($listOfQuizz as $currentQuiz) :?>
  <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
    <div class="card shadow ">
      <div class="card-body">
        <h4 class="card-title text-primary">
          <?= $this->e($currentQuiz->getTitle()); ?>
        </h4>
        <h5 class="card-text font-weight-bold">
          <?= $this->e($currentQuiz->getDescription()); ?>
        </h5>
        <p class="font-weight-light">
          <?= $this->e($currentQuiz->getFirstNameAuthor().' '.$currentQuiz->getLastNameAuthor()); ?>
        </p>
        <a href="<?=$router->generate('quiz_showQuiz', [
                    'id' => $this->e($currentQuiz->getId()),
                    ]); ?>" class="btn btn-primary">Commencer</a>
      </div>
    </div>
  </div>

  <?php endforeach; ?>
  <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
    <div class="card shadow btn" id="quizCreate">
      <div class="card-header">
        <div class="float-right ml-4 badge badge-success"></div>
      </div>
      <div class="card-body">
        <div class="text-center">
          <i class="fas fa-plus fa-4x text-primary"></i>
        </div>
      </div>
      <div class="card-footer">
      </div>
    </div>
  </div>
</div>