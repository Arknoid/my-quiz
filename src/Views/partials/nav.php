<a class="navbar-brand text-primary " href="<?=$router->generate('main_home'); ?>">
    <span class="display-4">O'Quiz</span>
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
    aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
    <ul class="navbar-nav">
        <?php if ($connectedUser !== false) : ?>
        <li class="nav-item">
            <p class="nav-link text-primary"> Bonjour <?= $connectedUser->getFirstName(); ?></p>
        </li>
        <?php endif; ?>
        <li class="nav-item ">
            <a class="btn btn-light text-primary" href="<?=$router->generate('main_home'); ?>">
                <i class="fas fa-home mr-1"></i>
                Accueil
            </a>
        </li>

        <?php if ($connectedUser !== false) : ?>
        <li class="nav-item">
            <a class="btn btn-light text-primary" href="<?=$router->generate('user_profile'); ?>">
                <i class="fas fa-user mr-1"></i>
                Mon compte
            </a>
        </li>
        <li class="nav-item">
            <a class="btn btn-light text-primary" href="<?=$router->generate('quiz_create'); ?>">
                <i class="fas fa-edit mr-1"></i>
                Crée un quiz
            </a>
        </li>
        <li class="nav-item">
            <a class="btn btn-light text-primary" href="<?=$router->generate('user_logout'); ?>">
                <i class="fas fa-sign-out-alt mr-1"></i>
                Déconnexion
            </a>
        </li>
        <?php else : ?>
        <li class="nav-item">
            <a class="btn btn-light text-primary" href="<?=$router->generate('user_signIn'); ?>">
                <i class="fas fa-sign-in-alt mr-1"></i>
                Connexion
            </a>
        </li>
        <li class="nav-item">
            <a class="btn btn-light text-primary" href="<?=$router->generate('user_signUp'); ?>">
                <i class="fas fa-user-plus mr-1"></i>
                Inscription
            </a>
        </li>
        <?php endif; ?>
    </ul>
</div>