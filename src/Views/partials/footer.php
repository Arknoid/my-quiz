<!-- Footer Links -->
<div class="container page-footer font-small indigo mt-4 text-muted">
    <div class="row text-center d-flex justify-content-center pt-5 mb-3">
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase font-weight-bold">
                <a href="<?=$router->generate('main_faq'); ?>" class="text-muted">F.A.Q</a>
            </h6>
        </div>
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase font-weight-bold">
                <a href="<?=$router->generate('main_cgu'); ?>" class="text-muted">C.G.U</a>
            </h6>
        </div>
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase font-weight-bold">
                <a href="#!" class="text-muted">Contact</a>
            </h6>
        </div>
    </div>
    <hr class="rgba-white-light" style="margin: 0 15%;">
    <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">
    </div>
    <hr class="clearfix d-md-none rgba-white-light">
    <div class="row  d-flex text-center justify-content-center pb-3 mt-4">
        <div class="col-md-12">
            <div class="mb-5 flex-center">
                <a href="#" class="text-muted">
                    <i class="fab fa-facebook fa-2x white-text mr-md-4"> </i>
                </a>
                <a href="#" class="text-muted">
                    <i class="fab fa-twitter fa-2x white-text mr-md-4 "> </i>
                </a>
                <a href="#" class="text-muted">
                    <i class="fab fa-google-plus fa-2x white-text mr-md-4"> </i>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Copyright -->
<div class="text-center mb-3">© Copyright 2018 : Oclock</div>