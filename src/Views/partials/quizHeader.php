<div class="jumbotron jumbotron-fluid shadow rounded m-4">
    <div class="container">
        <div>
            <h2>
                <span class="display-4"><?= $this->e($title); ?></span>
                <span class="ml-4 badge badge-pill badge-secondary"><?= $this->e($numberOfQuestions); ?> questions</span>
            </h2>
        </div>
        <h3 class="display-5"><?= $this->e($description); ?></h3>
        <p class="font-weight-light mt-4"><?= $this->e($author); ?></p>
    </div>
</div>