<?php $this->layout('layout', ['title' => 'Oquiz']); ?>
<div class="jumbotron jumbotron-fluid shadow rounded m-4">
    <div class="container">
        <h1 class="display-5 ">Bienvenue sur O'Quiz</h1>
        <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, expedita? Iusto quae earum saepe eligendi voluptates
            dolores placeat, ipsam aperiam ut, sed doloremque numquam quia iure quaerat facilis. Aliquid placeat iure numquam
            iusto veniam inventore atque possimus porro dicta qui adipisci cumque beatae ut voluptatibus nostrum quam quibusdam,
            dolor consectetur?</p>
    </div>
</div>

<div class="row m-3">
    <?php foreach ($listOfQuizz as $currentQuiz) :?>
    <div class="col-sm-6 col-lg-4 col-xl-3 pt-3">
        <div class="card shadow ">
            <div class="card-body">
                <h4 class="card-title text-primary"><?= $this->e($currentQuiz->getTitle()); ?></h4>
                <h5 class="card-text font-weight-bold"><?= $this->e($currentQuiz->getDescription()); ?></h5>
                <p class="font-weight-light"><?= $this->e($currentQuiz->getFirstNameAuthor().' '.$currentQuiz->getLastNameAuthor()); ?></p>
                <a href="<?=$router->generate('quiz_showQuiz', [
                    'id' => $this->e($currentQuiz->getId()),
                    ]); ?>" class="btn btn-primary">Commencer</a>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>