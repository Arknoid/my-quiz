<?php $this->layout('layout', ['title' => 'CGU']); ?>
<div class="jumbotron jumbotron-fluid shadow rounded m-5 ">
    <div class="container text-center">
        <h1 class="display-4">Condition général d'utilisation</h1>
    </div>
</div>
<div class="container d-flex justify-content-center">
    <p class="mt-1 col-lg-8">
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Suscipit, eius laborum consequatur provident commodi consectetur
        maiores porro corrupti nulla accusantium? Laborum amet inventore ab quis consectetur eaque officiis repellat, alias
        nostrum? Dolore nemo minus explicabo veniam ipsum modi beatae esse perspiciatis soluta inventore eveniet obcaecati
        a, quasi sed! Odio neque exercitationem rerum obcaecati, sit totam dolorem deleniti fuga molestiae quae sed voluptatem
        sequi, id nesciunt inventore sapiente eveniet pariatur dolore. Amet dolore libero vel alias, ad quidem laudantium
        iusto mollitia unde perspiciatis dolorem totam nesciunt hic consequatur expedita ratione nisi. Alias optio modi quibusdam,
        inventore facere ut doloribus asperiores eveniet id at. Adipisci voluptatum rem, fugit velit nesciunt provident,
        animi nulla dolorum vitae obcaecati optio alias autem eveniet illo mollitia, quis nemo explicabo voluptatibus eius
        earum. Magnam vitae itaque facilis aliquid totam blanditiis modi sapiente, tempore minus sequi voluptatum sed. Illo
        voluptatem earum ipsum tenetur, fugiat, voluptates qui magni eum quod placeat alias, amet aspernatur et! Explicabo
        quia eveniet fugit et doloremque laboriosam sunt soluta asperiores a at facere esse expedita ea corporis perferendis
        repellat, laudantium quasi id debitis voluptatum totam perspiciatis, dicta cumque aspernatur. Obcaecati est tempore
        consequatur odit asperiores! Repudiandae obcaecati aperiam veritatis labore eos excepturi, quo vitae velit distinctio
        atque itaque porro architecto nemo quasi unde nihil nobis. Obcaecati, totam laborum. Eveniet deleniti minus quis
        beatae repellat omnis architecto dolor velit, doloribus voluptates saepe temporibus corporis ratione sint ipsam sit
        vel, esse quia. Eum quod modi ea odit, iure, quas reiciendis quam, deserunt minima dolorum sed. Facilis laudantium
        architecto animi voluptas dolorum eaque, suscipit accusantium obcaecati non perferendis magnam porro enim tempore
        repudiandae ad quae dolorem, autem laborum! Commodi esse laudantium doloremque omnis delectus illo ipsam dignissimos
        amet blanditiis labore nisi quisquam, nulla enim tempora, ipsa dicta beatae vitae repellat fugiat similique, sit
        vero deleniti repellendus animi! Est nisi repellat iure accusantium sapiente, deserunt sunt voluptates tenetur inventore,
        hic facilis unde illum sed exercitationem quo veniam architecto. Quia perspiciatis sunt, consequuntur possimus sit
        illum eius non deserunt ducimus aspernatur tempora libero asperiores nobis dolores voluptates temporibus obcaecati
        aperiam voluptatum assumenda. Ea placeat laborum perferendis incidunt aliquam deleniti enim aut architecto pariatur
        totam asperiores beatae magnam sit dolore dolores modi, commodi officiis nesciunt. Sint esse perferendis quo impedit
        quae. Vitae omnis, voluptatibus repellendus itaque fugit quaerat eaque laboriosam aliquid amet atque nam numquam?
        Eos, amet eveniet voluptatibus quasi nesciunt sunt, facilis dolores eius laborum ducimus itaque, vitae maiores molestiae
        modi exercitationem commodi quam? Velit, ea? Enim debitis maxime doloremque consequatur minus expedita dolores omnis
        dolorum accusantium cumque id blanditiis harum rerum, dolor eum maiores architecto sapiente. Ut dolore deserunt iusto
        id porro quod beatae explicabo reprehenderit quaerat. Amet incidunt ad magni reprehenderit ratione enim maxime exercitationem
        illum, minus, soluta facere quos. Facere eum placeat error provident unde odio nesciunt rem dolorum laudantium! Nulla
        quod et illum corporis omnis. Praesentium cupiditate blanditiis corporis voluptatum labore esse accusantium. Numquam
        accusantium architecto, quo facilis dignissimos repellat eaque. Magnam, vero. Excepturi dolore aliquam quam voluptate
        nam aut quo enim! Quo, quod ipsa?
    </p>
</div>